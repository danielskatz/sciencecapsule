'use strict';
        
// Show an element
var show = function (elem) {
    elem.classList.add('is-visible');
};

// Hide an element
var hide = function (elem) {
    elem.classList.remove('is-visible');
};

// Toggle element visibility
var toggle = function (elem) {
    elem.classList.toggle('is-visible');
};

function toggleEventProperties(parent) {
    let elem = parent.querySelector('.event-properties')
    toggle(elem)
}

function showAll(selector) {
    document.querySelectorAll(selector).forEach(show)
}

function hideAll(selector) {
    document.querySelectorAll(selector).forEach(hide)
}

function getHTMLForEvent(event) {

    var propertiesOverview = `
        <div class="toggle-content event-properties">
            <p>
                <br>artifact: ${event.artifact},
                <br>artifact_type: ${event.artifact_type},
                <br>event_type: ${event.event_type},
                <br>event_time: ${event.event_time},
                <br>event_src: ${event.event_src},
                <br>snapshot_id: ${event.snapshot_id},
            </p>
        </div>
    `;

    var eventContentDisplay = ""
    var eventOrigin = 'experiment'
    
    var tsDisplayOptions = {timeZoneName: 'short', timeStyle: 'full'}
    
    switch (event.artifact_type) {
        case "EXE":
            case "DATA":
            let timestamp = new Date(event.event_time)
            let timestampDisplay = timestamp.toLocaleString(tsDisplayOptions)
            // TODO this should probably check if `event_uri` is given,
            // and then add some logic dealing with how to render the content depending on (mime)type, etc
            // eventContentDisplay = `<img src="capsule-data/${event.artifact}"/>`
            // for the moment, a link (opening in a new tab/window) could be a simple and more versatile alternative
            // eventContentDisplay = `<a href="capsule-data/${event.artifact}" target="_blank">${event.artifact}</a>`
            eventContentDisplay = `<code>${event.artifact}</code>&nbsp${event.event_type}&nbsp${timestampDisplay}`

            break;
        case "NOTE":
            eventContentDisplay = `${event['content']['html']}`
            eventOrigin = 'curation'
            break;
        default:
            break;
        // code block
    }

    var eventID = event._id != null ? event._id : ""

    var html = `
    <div onClick="toggleEventProperties(this)" id="${eventID}" class="content-block experiment ${event.artifact_type}" data-event-origin="${eventOrigin}" data-event-id="${eventID}">
        
        ${eventContentDisplay}
        ${propertiesOverview}
    </div>
    `;

    html += `<!-- Add Artifact -->
        <div class="add-artifact-wrapper">
            <div class="artifact-button-block">
                <input type="button" class="artifact-button artifact-button-local" onClick="setupAddArtifactArea(this)" value="+"/>
            </div>
        </div>`;

    return html.trim();
}

// Toggle function to hide/show image
function toggle_visibility(id) {
    var e = document.getElementById(id);
    if (e.style.display == 'none')
        e.style.display = 'block';
    else
        e.style.display = 'none';
}

function setupAddArtifactArea(buttonElem) {
    // let buttonElem = ev.target
    // console.log(ev)
    console.log('buttonElem:')
    console.log(buttonElem)
    let wrapper = buttonElem.closest('.add-artifact-wrapper')
    console.log(wrapper)
    let initialEditorHTML = `
        <div id="toolbar">
            <span class="ql-formats">
                <button class="ql-bold"></button>
                <button class="ql-italic"></button>
                <button class="ql-underline"></button>
            </span>
            <span class="ql-formats">
                <button class="ql-image"></button>
            </span>
            <button id="add-artifact-save">Save</button>
        </div>
        <div id="editor">
            <p>Hello World!</p>
            <p>Adding some info.</p>
            <p><br></p>
        </div>
    `.trim();

    // Add initialEditorHTML inside wrapper
    wrapper.insertAdjacentHTML('afterbegin', initialEditorHTML)

    let quill = new Quill(wrapper.querySelector('#editor'), {
        modules: {
            toolbar: wrapper.querySelector('#toolbar')
        },
        theme: 'snow'
    });

    var elSaveButton = wrapper.querySelector('#add-artifact-save')
    elSaveButton.addEventListener('click', e => dispatchAddedContent(quill, wrapper))
}

function dispatchAddedContent(editor, wrapperElem) {
    var note = getNotePayloadFromEditor(editor);
    addNoteContextFromSiblingElems(note, wrapperElem);
    addNoteContentToUI(note, wrapperElem);
    saveNoteToDB(note);
    teardownAddArtifactArea(wrapperElem);
}

function getNotePayloadFromEditor(editor) {
    var data = {
        artifact_type: 'NOTE',
        event_type: 'NOTE_CREATED',
        event_time: new Date().toISOString(),
        content: {
            quill_delta: {},
            text_plain: '',
            html: ''
        },
        context: {},
    }

    data.content['quill_delta'] = editor.getContents()
    data.content['text_plain'] = editor.getText()
    data.content['html'] = editor.root.innerHTML

    return data
}

function findNearestMatchingSibling(element, selector, getSibling) {
    let sel = String(selector)
    let sibling = getSibling(element)
    if (!sel) return sibling
    let nAttempts = 0
    while (sibling && nAttempts < 20) {
        // console.log('sibling: ')
        // console.log(sibling)
        if (sibling.matches(sel)) return sibling
        sibling = getSibling(sibling)
        nAttempts += 1
    }
}

function findPrevMatchingSibling(element, selector) { return findNearestMatchingSibling(element, selector, e => e.previousElementSibling) }
function findNextMatchingSibling(element, selector) { return findNearestMatchingSibling(element, selector, e => e.nextElementSibling) }

function addNoteContextFromSiblingElems(note, editorWrapperElem) {
    const sel = '.content-block.experiment[data-event-origin="experiment"]'
    let elClosestEventFuture = findPrevMatchingSibling(editorWrapperElem, sel)
    // console.log('elClosestEventFuture: ')
    // console.log(elClosestEventFuture)
    let elClosestEventPast = findNextMatchingSibling(editorWrapperElem, sel)
    // console.log('elClosestEventPast: ')
    // console.log(elClosestEventPast)

    note.context['closest_future_event_id'] = elClosestEventFuture.getAttribute('data-event-id')
    note.context['closest_past_event_id'] = elClosestEventPast.getAttribute('data-event-id')
}

function addNoteContentToUI(note, editorWrapperElem) {
    let html = getHTMLForEvent(note)
    // insert note card as a sibling of the editor
    editorWrapperElem.insertAdjacentHTML('afterend', html)
}

async function saveNoteToDB(note) {
    let reqOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrer: 'no-referrer',
        body: JSON.stringify(note)
    };

    let resp = fetch(getURL('/notes'), reqOptions
    ).then((resp) => {
        console.log('request was successful!')
        return resp.json();
    }).catch((err) => {
        throw error;
    });
}

function teardownAddArtifactArea(editorWrapperElem) {
    console.log(`Tearing down elem:`)
    console.log(editorWrapperElem)
    editorWrapperElem.querySelector('#editor').remove()
    editorWrapperElem.querySelector('#toolbar').remove()
}

// Scroll to specific values
// scrollTo is the same
window.scroll({
    top: 2500,
    left: 0,
    behavior: 'smooth'
});

// Scroll certain amounts from current position 
window.scrollBy({
    top: 100, // could be negative value
    left: 0,
    behavior: 'smooth'
});


const element = document.getElementById('chart');
const content = document.getElementById("content");
var eventData = [];

const BASE_URL = window.location.href

const stripLastCharIfPresent = (s, ch) => s.endsWith(ch) ? s.slice(0, -1) : s

const getURL = (endpoint) => {
    let base_url = stripLastCharIfPresent(BASE_URL, '/')
    let url = `${base_url}${endpoint}`
    console.log(`endpoint: ${endpoint} -> url: ${url}`)
    return url
}

async function getData() {
    let jsonResponse = await fetch(getURL('/events'), {
        headers: {
            'Content-Type': 'application/json',
        }
    }).then((response) => {
        return response.json();
    }).catch((error) => {
        throw (error);
    });
    jsonResponse.sort((a, b) => (b['event_time'] > a['event_time']) ? 1 : -1);

    let eventsTimelineData = processData(jsonResponse)

    content.innerHTML = `
    <h1 class="app-title">Events (results)</h1>
    ${jsonResponse.map(getHTMLForEvent).join("")}
    `;

    let notesData = await getDataNotes()
    notesData.forEach(addNoteNextToEvent)

    let notesTimelineData = [
        {
            // this will put note items on a separate area in the widget
            label: 'Notes',
            data: getTimelineItemsFromNotes(notesData, jsonResponse)
        }
    ]

    let timelineData = eventsTimelineData.concat(notesTimelineData)

    const timeline = new TimelineChart(element, timelineData, {
        tip: function (d) {
            return d.label;
        },
        intervalMinWidth: 4,
    }
    )
    // ).onVizChange(e => console.log(e));
}

async function getDataNotes() {
    let jsonResponse = await fetch(getURL('/notes'), {
        headers: {
            'Content-Type': 'application/json',
        }
    }).then((response) => {
        return response.json();
    }).catch((error) => {
        throw (error);
    });
    return jsonResponse
}

function addNoteNextToEvent(note) {
    let eventID = note['context']['closest_future_event_id']
    let sel = `.content-block.experiment[data-event-id="${eventID}"`
    let elem = document.querySelector(sel).nextElementSibling
    let html = getHTMLForEvent(note)
    elem.insertAdjacentHTML('afterend', html)
}

function getTimelineItemsFromNotes(notes, events) {
    let items = []

    notes.forEach(note => items.push(getTimelineItemFromNote(note, events)))

    return items
}

function getTimelineItemFromNote(note, events) {
    let futureEventID = note.context.closest_future_event_id
    let pastEventID = note.context.closest_past_event_id

    // TODO this is effectively a JOIN on the events "table"
    // we should probably consider doing it on the backend instead
    let futureEvent = events.find(e => e._id === futureEventID)
    let pastEvent = events.find(e => e._id === pastEventID)

    let item = {
        type: TimelineChart.TYPE.INTERVAL,
        from: new Date(pastEvent['event_time']),
        to: new Date(futureEvent['event_time']),
        label: note.content.html,
        customClass: 'orange-interval',
        $oid: note._id
    }

    return item
}

function processData(responseData) {
    const filter = /.pickle$/;
    let groups = new Map();

    eventData = responseData.map((current) => {
        if (!groups.has(current['snapshot_id'])) {
            groups.set(current['snapshot_id'], []);
        }

        let data = {};

        let currentArtifact = current['artifact']
        if (!currentArtifact) {
            console.log('WARNING: encountered malformed item')
            console.log(JSON.stringify(current))
            currentArtifact = "<unknown>"
        }

        if (!currentArtifact.match(filter)) {
            let values = groups.get(current['snapshot_id']);
            let single_timestamp = new Date(current['event_time']);

            data['type'] = TimelineChart.TYPE.POINT;
            data['at'] = single_timestamp;

            data['type'] = TimelineChart.TYPE.INTERVAL;
            data['from'] = single_timestamp;
            data['to'] = single_timestamp;

            let label = `${current['event_type']} ${currentArtifact}<br>at ${current['event_time']}`;
            data['label'] = label;

            if (current["_id"] != null) {
                // this has to match the onclick event on the Timeline items
                data['$oid'] = current["_id"];
            }

            // Color by type field
            if (current['artifact_type'] == 'DATA') {
                data['customClass'] = 'purple-interval';
            }

            values.push(data);
            groups.set(current['snapshot_id'], [...values]);
        }

        return current;
    })

    let keys = [...groups.keys()]
    let timeline_data = keys.map((key) => {
        return {
            label: key,
            data: [...groups.get(key)]
        }
    })

    return timeline_data;
}

getData();






