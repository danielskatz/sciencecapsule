from unittest import mock

import mongomock

import pytest


@pytest.fixture(scope='module')
def mock_db_uri(request):

    db_host = request.node.name.replace('/', '.')
    db_name = 'test'
    mongo_uri = f'mongodb://mongomock.{db_host}/{db_name}'

    ctx_info = f'mocked context for DB URI {mongo_uri}'

    with mock.patch('pymodm.connection.MongoClient', new=mongomock.MongoClient):
        print(f'\nInside {ctx_info}')
        yield mongo_uri

    print(f'\nExited {ctx_info}')
