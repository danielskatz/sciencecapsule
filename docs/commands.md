# Science Capsule commands

This documents displays a list of the Science Capsule commands along with their respective options.

It is primarily intended as a reference;
for more information on the functionality provided by these commands, refer to the rest of the Science Capsule documentation.

### sc services

Manage Science Capsule services.

#### Usage

```sh
sc services [-h] [--inspect-processes] [<action>] ...
```

#### Positional arguments

```txt
  <action>             Monitoring action to perform. Leave empty for interactive use, or use the `help` action to see all available actions.
  action_args          Arguments to pass to the action
```

#### Actions

See `sc services help <action>` for more details about a specific action.

```txt
start <name>          Start a process
start <gname>:*       Start all processes in a group
start <name> <name>   Start multiple processes or groups
start all             Start all processes
stop <name>           Stop a process
stop <gname>:*        Stop all processes in a group
stop <name> <name>    Stop multiple processes or groups
stop all              Stop all processes
status <name>         Get status for a single process
status <gname>:*      Get status for all processes in a group
status <name> <name>  Get status for multiple named processes
status                Get all process status info
shutdown              Shut the remote supervisord down.
tail [-f] <name> [stdout|stderr] (default stdout)
tail -f <name>        Continuous tail of named process stdout. Ctrl-C to exit.
tail -100 <name>      last 100 *bytes* of process stdout
tail <name> stderr    last 1600 *bytes* of process stderr
```

#### Optional arguments

```txt
  -h, --help           Show this help message and exit
  --inspect-processes  Show information about running internal processes and exits.
```

### sc inspect

Inspect Science Capsule data and configuration.

```sh
sc inspect [-h] [-f {json,table}] [-s [<field> [<field> ...]]] [-i [<field> [<field> ...]]] [-e [<field> [<field> ...]]] [-l N] {events,events.filesystem,events.process,raw_events.watchdog,raw_events.inotify,raw_events.strace}
```

#### Positional arguments

```txt
{events,events.filesystem,events.process,raw_events.watchdog,raw_events.inotify,raw_events.strace}      Name of the resource to inspect
```

#### Optional arguments

```txt
-h, --help                                                                  Show this help message and exit
-f {json,table}, --format {json,table}                                      Format to use to display the data
-s [<field> [<field> ...]], --sort-fields [<field> [<field> ...]]           Space-separated list of fields to sort by. Use a trailing dash "-" to indicate descending order
-i [<field> [<field> ...]], --include-only-fields [<field> [<field> ...]]   Only display the given subset of fields
-e [<field> [<field> ...]], --exclude-fields [<field> [<field> ...]]        Exclude the given subset of fields
-l N, --limit N                                                             Limit the output to N entries
```

### sc bootstrap

Create and populate Science Capsule configuration and services.

```sh
sc bootstrap [-h] [-d DIR] [-s EVENT_SOURCES] config_dir
```

#### Positional arguments

```txt
config_dir            The directory that will be created to contain the configuration. To avoid overwriting existing directories, specifying an existing directory is not allowed.
```

#### Optional arguments

```txt
  -h, --help                                              Show this help message and exit
  -d DIR, --monitored-dir DIR                             Directory to be monitored. For multiple directories, specify this option multiple times, e.g. "-d /path/to/dir1 -d /path/to/dir2"
  -s EVENT_SOURCES, --event-sources EVENT_SOURCES         Event source(s) to use, separated by comma for multiple sources, e.g. "--event-sources sourceA,sourceB". If "auto" (default), event sources supported by the current target will be chosen automatically.
```

### sc capture

Manage and trigger capture from available event sources.

#### Usage

```sh
sc capture [-h] [--monitor] [--no-ingest]
```

#### Optional arguments

```txt
-h, --help   Show this help message and exit
--monitor    Enable real-time monitoring for event capture.
--no-ingest  Do not ingest events (including analysis of existing raw events)
```

---
