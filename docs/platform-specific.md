## Platform-specific examples and possible issues

The following section describes specific examples and workarounds to possible issues, particularly for Windows systems.

### Using the `sc` command-line tool on Windows

Depending on the Windows version and the shell being used, the `sc` command might resolve to a different command or alias instead of the Science Capsule `sc` executable.

If this is the case (e.g. errors mentioning invalid arguments when using `sc`), one workaround that is known to be working is to add the `.exe` extension to invoke the Science Capsule executable, i.e. use `sc.exe` instead of `sc` for any of the commands shown in this document.

### Managing Science Capsule services on Windows using Powershell

**NOTE** The following commands assume that

- Science Capsule was installed in a Conda environment named `sciencecapsule`
- Science Capsule was initialized using the `sc bootstrap` command in a directory at `C:\Users\myuser\.scicap`
- The `sciencecapsule` Conda environment has been activated for the current shell session

---

Set the `SC_CONFIG_DIR` environment variable:

```
(sciencecapsule) PS C:\Users\myuser\workdir> $env:SC_CONFIG_DIR = 'C:\Users\myuser\.scicap'
```

Verify that the `SC_CONFIG_DIR` environment variable is set:

```
(sciencecapsule) PS C:\Users\myuser\workdir> Get-ChildItem Env:SC_CONFIG_DIR

Name			Value
----			-----
SC_CONFIG_DIR		C:\Users\myuser\.scicap

```

Verify that the directory to which `SC_CONFIG_DIR` points to exists and has the correct contents:

```
(sciencecapsule) PS C:\Users\myuser\workdir> dir $env:SC_CONFIG_DIR

    Directory: C:\Users\myuser\.scicap


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
d-----        12/3/2020   9:14 PM                data
d-----        5/26/2021   8:46 PM                services
-a----        12/3/2020   9:14 PM             70 events.yml
-a----        12/3/2020   9:14 PM            112 mongo_uri

```

Start the Science Capsule services:

```
(sciencecapsule) PS C:\Users\myuser\workdir> sc.exe services start all
```
