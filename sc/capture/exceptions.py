

class ScienceCapsuleException(Exception):
    "Base class for exceptions created by Science Capsule."


class ConfigurationError(ScienceCapsuleException):
    "Represents an error in accessing or validating Science Capsule configuration values"


class DatabaseError(ScienceCapsuleException):
    "Represents an error in a Science Capsule database interaction"


class NothingToProcess(ScienceCapsuleException):
    """
    Signals the absence of input data to further processing, e.g.
    in cases where there are no raw events,
    or filtering criteria within a processing chain result in an empty selection.
    """


class MonitorStatusException(ScienceCapsuleException):
    """
    Used to signal to monitor clients any issue preventing normal operation.
    """


class ProcessingError(ScienceCapsuleException):

    def __init__(self, *args, debug_info=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.debug_info = debug_info


class DatabaseLoadError(DatabaseError):
    pass


class DatabaseSaveError(DatabaseError):
    pass


class ReceivedStopSignal(BaseException):
    def __init__(self, sig_num=None):
        self.sig_num = sig_num
