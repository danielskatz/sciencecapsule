import psutil
import pandas as pd


def _is_internal_process(pinfo):
    maybe_env = pinfo.get('environ')

    # unlike a normal missing key, pinfo already has the 'environ' key, but its value will be None
    env = maybe_env if maybe_env is not None else {}

    # sentinel_value = env.get('SC_CONFIG_DIR')
    sentinel_value = env.get('SC_INTERNAL_PROCESS')

    return sentinel_value is not None


def _get_internal_processes_info():
    attrs = ['name', 'pid', 'cmdline', 'environ', 'create_time']
    procs = list(p.info for p in psutil.process_iter(attrs) if _is_internal_process(p.info))

    return procs


def _get_timestamp_from_epoch(s, unit='s'):

    return (pd.to_datetime(s, origin='unix', unit=unit)
            .dt.tz_localize('UTC')
            )


# TODO change default timezone
def _to_human_timestamp(s_epoch, timezone='US/Pacific'):

    return (s_epoch
            .pipe(_get_timestamp_from_epoch)
            .dt.tz_convert(timezone)
            .dt.strftime(r'%c')
           )


def _display_internal_processes():

    procs = _get_internal_processes_info()

    df = pd.DataFrame(procs)

    if df.empty:
        print('No internal processes found.')
        return

    df = df.assign(
        create_time=lambda d: d['create_time'].pipe(_to_human_timestamp),
        cmdline=lambda d: d['cmdline'].str.join(' '),
        SC_CONFIG_DIR=lambda d: d['environ'].apply(lambda env: env.get('SC_CONFIG_DIR', '<none>')),
    )[['name', 'pid', 'create_time', 'SC_CONFIG_DIR', 'cmdline']]

    with pd.option_context('display.max_colwidth', -1):
        print(df)


if __name__ == "__main__":
    _display_internal_processes()
