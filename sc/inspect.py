import argparse
import json
import sys

from sc.db import models
from sc.capture import sources


MODELS = {
    'events': models.Event,
    'events.filesystem': models.filesystem.Event,
    'events.process': models.process.Event,
    'raw_events.watchdog': sources.watchdog.WatchdogRawEvent,
    'raw_events.inotify': sources.inotify.InotifyRawEvent,
    'raw_events.strace': sources.strace.StraceRawEvent,
}


FORMATS = {
    'json': lambda qs, **kwargs: qs.to_json(**kwargs),
    'table': lambda qs, **kwargs: qs.to_dataframe(normalize=True, nested_field_sep='.', **kwargs)
}


def display_model(key,
                  format=None, file=sys.stdout,
                  sort_fields=None, only_fields=None, exclude_fields=None, limit=None,
                  **kwargs
    ):

    try:
        model = MODELS[key]
    except KeyError as e:
        raise ValueError(f'Invalid value "{key}". Valid choices are: {list(MODELS)}') from e

    qs = model.objects.all()

    if sort_fields:
        qs = qs.order_by_field(sort_fields)

    if only_fields:
        qs = qs.only(*only_fields)

    if exclude_fields:
        qs = qs.exclude(*exclude_fields)

    if limit:
        qs = qs.limit(limit)

    models.connect()

    func = FORMATS.get(format)
    to_display = func(qs, **kwargs)

    print(to_display, file=file)


def populate_cli_parser(parser: argparse.ArgumentParser):
    parser.add_argument('resource',
        help='Name of the resource to inspect',
        choices=list(MODELS)
    )

    parser.add_argument('-f', '--format',
        help='Format to use to display the data',
        choices=list(FORMATS),
        default='table'
    )

    parser.add_argument('-s', '--sort-fields',
        help='Space-separated list of fields to sort by. Use a trailing dash "-" to indicate descending order',
        nargs='*',
        metavar='<field>'
    )

    parser.add_argument('-i', '--include-only-fields',
        help='Only display the given subset of fields',
        nargs='*',
        metavar='<field>',
    )

    parser.add_argument('-e', '--exclude-fields',
        help='Exclude the given subset of fields',
        nargs='*',
        metavar='<field>'
    )

    parser.add_argument('-l', '--limit',
        type=int,
        metavar='N',
        help='Limit the output to N entries',
    )

    def _dispatch_func(args):
        display_model(
            key=args.resource,
            format=args.format,
            sort_fields=args.sort_fields,
            only_fields=args.include_only_fields,
            exclude_fields=args.exclude_fields,
            limit=args.limit
        )

    parser.set_defaults(
        dispatch_func=_dispatch_func
    )

    return parser


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser()
    populate_cli_parser(parser)

    args = parser.parse_args()
    args.dispatch_func(args)


if __name__ == "__main__":
    main()
